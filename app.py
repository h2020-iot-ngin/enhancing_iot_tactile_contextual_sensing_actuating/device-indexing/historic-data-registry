from huey import RedisHuey

from flask import Flask
from flask_pymongo import PyMongo
import os

from flask_restx import Api

app = Flask(__name__)

api = Api(app, version='1.2.3', title='IoT-NGIN Historic Data Registry',
    description='Get past mesurements of IoT Devices',
    doc='/swagger'
)

user = os.getenv('MONGO_INITDB_ROOT_USERNAME')
password = os.getenv('MONGO_INITDB_ROOT_PASSWORD')
db_name = os.getenv('MONGO_INITDB_DATABASE')
db_host = os.getenv('MONGO_HOST')
db_port = os.getenv('MONGO_PORT')

uri = f"mongodb://{user}:{password}@{db_host}:{db_port}/{db_name}?authSource=admin"

app.config['MONGO_URI'] = uri

client = PyMongo(app)
db = client.db

host = os.getenv('REDIS_HOST')
huey = RedisHuey(host=host)