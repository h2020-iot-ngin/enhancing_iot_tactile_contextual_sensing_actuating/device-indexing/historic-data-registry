# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Historic Data Registry.
#
# Historic Data Registry is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Historic Data Registry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Historic Data Registry. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

from flask import Flask, request, jsonify, Response
from flask_pymongo import PyMongo
import os
import json

import tasks

from flask_restx import Resource

from app import app

from app import db

from app import huey

from swagger_models import * 

@hello_message.route('', methods=['GET'])
class HelloMessage(Resource):

    @hello_message.doc(responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' }, 
                     description="Hello Message") 
    def get(self):
        message = {"message": "Hello this is the Historic Data Registry of the IoT-NGIN Device Indexing Module!"}
        response = json.dumps(message)

        return Response(response, mimetype='application/json')
    

@notification.route('', methods=['POST'])
class Notification(Resource):
    """
    POST the Orion notification with the latest measurements of a Synfield node
    """

    @notification.doc(parser=parser, 
               responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' },
               description="Receive Orion notification with latest measurement of IoT Device", 
               body=notification_body)
    def post(self):
        if request.method == 'POST':
            if request.data:    
                notification = request.get_json()

                if 'Fiware-service' in request.headers:
                    fiware_service = request.headers['Fiware-service']
                    fiware_service_path = request.headers['Fiware-Servicepath']
                elif 'Ngsild-Tenant' in request.headers:
                    fiware_service = request.headers['Ngsild-Tenant']
                    fiware_service_path = request.headers['Fiware-Servicepath']

                message = tasks.queue_notifications(notification, fiware_service, fiware_service_path)

                print(message(blocking=True, timeout=10), flush=True)
                # response = json.dumps(message(blocking=True, timeout=5))
                response = json.dumps({"message": "Added to db!"})

                return Response(response, mimetype='application/json')

 

@get_all_records.route('', methods=['GET'])
class GetAllRecords(Resource):
    """
    GET all the records stored in MongoDB
    """

    @get_all_records.doc(parser=parser, 
                     responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' }, 
                     description="Get all stored records")
    def get(self):
        if request.method == 'GET':
            if 'Fiware-service' in request.headers and 'Fiware-servicepath' in request.headers:
                fiware_service = request.headers['Fiware-service']
                fiware_service_path = request.headers['Fiware-servicepath']

                service = db[fiware_service]
                service_path = service[fiware_service_path]

                query = {}
                records = service_path.find(query, {'_id': False})
                count = service_path.count_documents(query)
                
                if count == 0:
                    message = {"message": "Empty DB!"}
                    response = json.dumps(message)
                else:
                    response = []
                    for record in records:
                        response.append(record)

                    response = json.dumps(response)
            else:
                message = {"message": "Please include headers 'Fiware-service' and 'Fiware-servicepath' in your request!"}
                response = json.dumps(message)

            return Response(response, mimetype='application/json')  


@get_records.route('/<string:entity_id>', methods=['GET'])
class GetRecords(Resource):
    """
    GET all records stored for a particular device in MongoDB
    """

    @get_records.doc(parser=parser, 
                     responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' }, 
                     description="Get all stored records for a paricular device",
                     params={ "filter": "The filter to query the attributes or sub-attributes" })
    def get(self, entity_id):
        if request.method == 'GET':
            if len(request.args) == 0:
                attribute = []
            else: 
                attribute = request.args['filter']

            
            if 'Fiware-service' in request.headers and 'Fiware-servicepath' in request.headers:
                fiware_service = request.headers['Fiware-service']
                fiware_service_path = request.headers['Fiware-servicepath']

                service = db[fiware_service]
                service_path = service[fiware_service_path]

                if attribute == []:
                    query = {"id": entity_id}
                    records = service_path.find(query, {'_id': False})
                else:
                    attribute_key = attribute.split("==")[0]
                    attribute_value = attribute.split("==")[1]

                    if attribute_value.replace('.', '', 1).isdigit():
                        query = {"id": entity_id, attribute_key: float(attribute_value)}
                        records = service_path.find(query, {'_id': False})
                    else:
                        query = {"id": entity_id, attribute_key: attribute_value}
                        records = service_path.find(query, {'_id': False})
                
                count = service_path.count_documents(query)
                if count == 0:
                    message = {"message": "There are no records for this entity or your headers are incorrect!"}
                    response = json.dumps(message)
                else:
                    response = []
                    
                    for record in records:
                        response.append(record)

                    response = json.dumps(response)
            else:
                message = {"message": "Please include headers 'Fiware-service' and 'Fiware-servicepath' in your request!"}
                response = json.dumps(message)

            return Response(response, mimetype='application/json')


# Example url: http://localhost/get_last_n_records/<entity_id>?last_n=<last_n>
@get_last_n_records.route('/<string:entity_id>', methods=['GET'])
class GetLastNRecords(Resource):
    """
    GET the last N records for a particular device in MongoDB
    """

    @get_last_n_records.doc(parser=parser, 
                     responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' }, 
                     description="Get the last N records for a paricular device, using its ID",
                     params={ "last_n": "The number of latest records" })
    def get(self, entity_id):
        if request.method == 'GET':
            if 'Fiware-service' in request.headers and 'Fiware-servicepath' in request.headers:
                num_of_last_records = request.args['last_n']

                fiware_service = request.headers['Fiware-service']
                fiware_service_path = request.headers['Fiware-servicepath']

                service = db[fiware_service]
                service_path = service[fiware_service_path]

                query = {"id": entity_id}
                count = service_path.count_documents(query)

                # https://stackoverflow.com/questions/4421207/how-to-get-the-last-n-records-in-mongodb
                skip_value = count - int(num_of_last_records)
                if skip_value >= 0:
                    records = service_path.find(query, {'_id': False}).skip(skip_value)
                else:
                    records = service_path.find(query, {'_id': False})

                if count == 0:
                    message = {"message": "There are no records for this entity or your headers are incorrect!"}
                    response = json.dumps(message)
                else:
                    response = []
                    for record in records:
                        response.append(record)
                    
                    response = json.dumps(response)
            else:
                message = {"message": "Please include headers 'Fiware-service' and 'Fiware-servicepath' in your request!"}
                response = json.dumps(message)

            return Response(response, mimetype='application/json')