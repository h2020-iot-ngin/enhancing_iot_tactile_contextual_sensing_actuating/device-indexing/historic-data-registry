# Device Indexing Registry

## Purpose
The Device Indexing Registry is part of the IoT-NGIN Indexing (IDI) module and is used to persist the data sent by the IoT devices to the Orion Context Broker, since the information kept by Orion includes only the latest update. The registry receives notifications for all the updates, stores the information and exposes different endpoints to interact with the data.

## Pre configuration
For the registry to be configured properly, a subscription to FIWARE Orion has to be made for the component to receive the latest notifications with a device's updates.
To create the appropriate subscription for the Historic Data Registry, one field to pay special attention to is the `notification.http.url`. Since the Registry works as a Kubernetes deployment, the URL can take two of the following forms:
* It can contain the DNS name or the IP that the component is exposed through(e.g. http://127.0.0.1:8080/notification)
* It can contain the name of the Kubernetes Deployment of the Historic Data Registry (e.g. in the case of Onelab the URL takes the following form: `http://device-indexing-registry.iot-ngin-wp4/notification`). This form helps keep the communication of the components internal and can prove useful in case there is limited information the IP of the Kubernetes node the component is deployed to or if it is not exposed through a DNS name

If you are using multitenancy, pay attention to the appropriate headers when making the request. For more details on the multitenacy feature, refer [here](https://thinking-cities.readthedocs.io/en/release-v4.0/multitenancy/). 

[Here](https://gitlab.com/h2020-iot-ngin/enhancing_iot_tactile_contextual_sensing_actuating/idi-idac-demo) is a script available to test craeting a subscription.

Note: The endpoint that the Historic Data Registry expects the notifications to is `/notification`. For more information about the available endpoints, refer [here](#api-endpoints)


## API endpoints

| Endpoint | HTTP Request | Explanation |
|   ---    |     ---      |     ---     |
| `/get_all_records` | `GET` | Get all the available records |
| `/get_records/<entity_id>?filter=<attribute/sub-attribute>==<values>` | `GET` | Get all the records which correspond to a particular entity id. Request argument `?filter` is optional, if the results need to be filtered according to an [attribute](https://www.postman.com/fiware/workspace/fiware-foundation-ev-s-public-workspace/request/513743-fcbde4dd-b8fa-42b7-bacf-158e7d6ecb1d) (e.g `?filter=location==2.35454`) or [sub-attribute](https://www.postman.com/fiware/workspace/fiware-foundation-ev-s-public-workspace/request/513743-2c5d1c0a-9d26-4422-8d07-8cf7ab5fa309) (e.g `?filter=item.id==45`) |
| `/notification` | `POST` | Receive notification sent by Orion Context Broker |
| `/get_last_n_records/<entity_id>?last_n=<last_n>`  | `GET` | Get the last n records stored in mongodb | 

## Run
To deploy the registry, execute the following command:
```bash
kubectl apply -f config/k8s/combo.yml
```