#!/bin/sh

VIRTUALENV_PATH=$(pipenv --venv)

pipenv run python3 $VIRTUALENV_PATH/bin/huey_consumer.py main.huey -w2