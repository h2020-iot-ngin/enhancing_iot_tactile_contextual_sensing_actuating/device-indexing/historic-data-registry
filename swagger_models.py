# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Historic Data Registry.
#
# Historic Data Registry is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Historic Data Registry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Historic Data Registry. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

from main import api
from flask_restx import fields

parser = api.parser()
parser.add_argument("Fiware-service", type= str, required=True, location="headers")
parser.add_argument("Fiware-servicepath", type= str, required=True, location="headers")

synfield_parser = api.parser()
synfield_parser.add_argument("Fiware-service", type= str, required=True, location="headers")


# Endpoint: "/"
hello_message = api.namespace("get_hello_message", description="Welcome message")

# Endpoint: "/notification"
notification = api.namespace("notification", description="Receive Orion notification with latest measurement of IoT Device")
notification_body = api.model("notification", {"subscriptionId": fields.String(required=True, description="The ID of the subscription"), 
                                          "data": fields.List(fields.Nested(api.model(
                                            "data", {
                                                    "id": fields.String(required=True, description="The ID of the device"),
                                                    "type": fields.String(required=True, description="The type of the device"),
                                                    "sensor_id": fields.String(required=False, description="The device ID"),
                                                    "ip": fields.String(required=False, description="The device IP"),
                                                    "metrics": fields.List(fields.Nested(api.model(
                                                        "metrics", 
                                                            {
                                                                "sensing_service_id": fields.Integer(required=False, description="The ID of the Sensing Service"), 
                                                                "sensing_service_name": fields.String(required=False, description="The name of the Sensing Service"),
                                                                "value": fields.String(required=False, description="The value of the sensor's measurement"),
                                                                "ontime": fields.DateTime(required=False, description="The time of the measurement collection")
                                                            }
                                                        
                                                    )))
                                                })))
                                         })


# Endpoint: /get_all_records
get_all_records = api.namespace("get_all_records", description="Get all stored records")

                                                
# Endpoint: /get_records/<entity_id>
get_records = api.namespace("get_records", description="Get all stored records for a paricular device, using its ID")


# Endpoint: /get_last_n_records/<entity_id>?last_n=<last_n>
get_last_n_records = api.namespace("get_last_n_records", description="Get the last N records for a paricular device, using its ID")
