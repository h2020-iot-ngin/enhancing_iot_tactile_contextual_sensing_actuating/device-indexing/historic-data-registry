# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Historic Data Registry.
#
# Historic Data Registry is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Historic Data Registry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Historic Data Registry. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#


from app import app
from app import api
from app import huey
import tasks  # Import tasks so they are registered with Huey instance.
import views  # Import views so they are registered with Flask app.

if __name__=='__main__':
    app.run(host="127.0.0.1", port=5000)