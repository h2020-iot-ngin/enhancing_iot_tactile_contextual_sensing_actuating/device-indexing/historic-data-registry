from app import huey, db
from flask import request

@huey.task()
def queue_notifications(notification, fiware_service, fiware_service_path):
    record = {}

    service = db[fiware_service]
    service_path = service[fiware_service_path]

    print(notification, flush=True)

    data = notification['data'][0]

    for key, value in data.items():
        record[key] = value

    service_path.insert_one(record)

    message = {"message": "Added to db!"}

    return message